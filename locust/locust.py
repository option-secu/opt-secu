from locust import HttpLocust, TaskSet, task, between
import random
import numpy as np

class UserBehavior(TaskSet):
    wait_time = between(5, 15)

    def on_start(self):
        self.login()

    def on_stop(self):
        self.logout()

    def login(self):
        response = self.client.get("http://localhost:8000/StoreManager/connexion")
        csrftoken = response.cookies['csrftoken']
        params = {"username":"Michel", "password":"password", "csrfmiddlewaretoken":csrftoken}
        self.client.post("http://localhost:8000/StoreManager/connexion", params, headers={"X-CSRFToken": csrftoken},
                     cookies={"csrftoken": csrftoken})

    def logout(self):
        response = self.client.get("http://localhost:8000/StoreManager/deconnexion")
        csrftoken = response.cookies['csrftoken']
        params = {"csrfmiddlewaretoken": csrftoken}
        self.client.post("http://localhost:8000/StoreManager/deconnexion", params, headers={"X-CSRFToken": csrftoken},
                         cookies={"csrftoken": csrftoken})

    # Liste des articles
    @task(1)
    def produits(self):
        self.client.get("http://localhost:8000/StoreManager/produit")

    # Modifications aléatoire de 1 à 9 produits
    @task(1)
    def produits_modifier(self):
        response = self.client.get("http://localhost:8000/StoreManager/produit")
        csrftoken = response.cookies['csrftoken']

        nb_produits = random.randint(0, 9)
        params = {"action_product": np.linspace(1, nb_produits, num=nb_produits), "csrfmiddlewaretoken":csrftoken}
        self.client.post("http://localhost:8000/StoreManager/produit", params, headers={"X-CSRFToken": csrftoken},
                     cookies={"csrftoken": csrftoken})

class WebsiteUser(HttpLocust):
    task_set = UserBehavior