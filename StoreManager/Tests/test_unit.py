"""
This file contains all the unit tests implemented in
Django
"""

from django.contrib.auth.models import User
from django.test import TestCase

from StoreManager.models import Employee, Department, Store, Product


class UnitTestsProduct(TestCase):
    """
    Unit tests on Product object
    """
    department1 = None

    @classmethod
    def setUpTestData(cls):
        """
        Generate data for testing
        """
        superuser = User.objects.create_superuser("superuser",
                                                  "fakemail2@mail.com",
                                                  "mdp")
        superuser.save()
        store = Store(name="store", user=superuser)
        store.save()
        cls.department1 = Department(name="d1", store=store)
        cls.department1.save()

        cls.product = Product(quantity=10, name="Pomme", price=2.5,
                              ref="APL2020", department=cls.department1)
        cls.product.save()

    def test_modify_quantity_ok(self):
        """
        Test modify with correct quantity
        """
        new_quantity = 20
        self.product.modify(quantity=new_quantity)
        self.assertEqual(new_quantity, self.product.quantity)

    def test_modify_quantity_float_ok(self):
        """
        Test modify with correct float quantity
        """
        new_quantity = 20.5
        self.product.modify(quantity=new_quantity)
        self.assertEqual(int(new_quantity), self.product.quantity)

    def test_modify_quantity_string_ok(self):
        """
        Test modify with correct string quantity
        """
        new_quantity = '20'
        self.product.modify(quantity=new_quantity)
        self.assertEqual(int(new_quantity), self.product.quantity)

    def test_modify_quantity_string_ko(self):
        """
        Test modify with incorrect string quantity
        """
        new_quantity = 'vingt'
        self.assertRaisesMessage(ValueError, 'format de la quantité invalide',
                                 self.product.modify, quantity=new_quantity)

    def test_modify_quantity_negative_ko(self):
        """
        Test modify with incorrect negative quantity
        """
        new_quantity = -10
        self.assertRaisesMessage(ValueError,
                                 'la quantité ne peut pas être négative',
                                 self.product.modify,
                                 quantity=new_quantity)

    def test_modify_name_ok(self):
        """
        Test modify with correct name
        """
        new_name = "poire"
        self.product.modify(name=new_name)
        self.assertEqual(new_name, self.product.name)

    def test_modify_name_empty_ko(self):
        """
        Test modify with incorrect empty name
        """
        new_name = ""
        self.assertRaisesMessage(ValueError, 'nom vide',
                                 self.product.modify, name=new_name)

    def test_modify_price_ok(self):
        """
        Test modify with correct price
        """
        new_price = 3
        self.product.modify(price=new_price)
        self.assertEqual(new_price, self.product.price)

    def test_modify_price_float_ok(self):
        """
        Test modify with correct float price
        """
        new_price = 3.5
        self.product.modify(price=new_price)
        self.assertEqual(new_price, self.product.price)

    def test_modify_price_string_ok(self):
        """
        Test modify with correct string price
        """
        new_price = '3.5'
        self.product.modify(price=new_price)
        self.assertEqual(float(new_price), self.product.price)

    def test_modify_price_string_ko(self):
        """
        Test modify with incorrect string price
        """
        new_price = "trois"
        self.assertRaisesMessage(ValueError, 'format du prix invalide',
                                 self.product.modify, price=new_price)

    def test_modify_price_negative_ko(self):
        """
        Test modify with incorrect negative price
        """
        new_price = -3.5
        self.assertRaisesMessage(ValueError,
                                 'le prix ne peut pas être négatif',
                                 self.product.modify, price=new_price)

    def test_modify_ref_ok(self):
        """
        Test modify with correct ref
        """
        new_ref = "0202LPA"
        self.product.modify(ref=new_ref)
        self.assertEqual(new_ref, self.product.ref)

    def test_modify_ref_empty_ko(self):
        """
        Test modify with incorrect empty ref
        """
        new_ref = ""
        self.assertRaisesMessage(ValueError, "reference vide",
                                 self.product.modify, ref=new_ref)

    def test_modify_ref_keep_ref_ok(self):
        """
        Test modify with self ref
        """
        new_ref = self.product.ref
        self.product.modify(ref=new_ref)
        self.assertEqual(new_ref, self.product.ref)

    def test_modify_ref_existing_ref_ko(self):
        """
        Test modify with incorrect existing ref
        """
        new_product = Product(quantity=10, name="Poire", price=2.5,
                              ref="POI2020", department=self.department1)
        new_product.save()
        new_ref = new_product.ref
        self.assertRaisesMessage(ValueError, "reference déja existante",
                                 self.product.modify, ref=new_ref)

    def test_modify_nothing_ok(self):
        """
        Test modify with without changing anything
        """
        name = self.product.name
        price = self.product.price
        quantity = self.product.quantity
        ref = self.product.ref
        self.product.modify()
        self.assertEqual(name, self.product.name)
        self.assertEqual(price, self.product.price)
        self.assertEqual(quantity, self.product.quantity)
        self.assertEqual(ref, self.product.ref)

    def test_modify_all_ok(self):
        """
        Test modify when changing everything
        """
        name = "petites pommes"
        price = 1.5
        quantity = 100
        ref = "APL2021"
        self.product.modify(name=name, price=price, quantity=quantity, ref=ref)
        self.assertEqual(name, self.product.name)
        self.assertEqual(price, self.product.price)
        self.assertEqual(quantity, self.product.quantity)
        self.assertEqual(ref, self.product.ref)

    def test_does_ref_exists_correct(self):
        """
        Tests does_ref_exists method when a product is correct
        """
        product = Product(quantity=10, name="Produit1", price=10.2,
                          ref="Ref")
        result = product.is_ref_valid(product.ref)
        self.assertEqual(True, result)

    def test_does_ref_exists_incorrect(self):
        """
        Tests does_ref_exists method when a reference already exists
        """
        existing_product = Product(quantity=10, name="Produit1",
                                   price=10.2,
                                   ref="Ref", department=self.department1)
        existing_product.save()
        product = Product(quantity=10, name="Produit2", price=10.2,
                          ref="Ref2",
                          department=self.department1)
        product.save()
        result = product.is_ref_valid(existing_product.ref)
        self.assertEqual(False, result)

    def test_does_ref_exists_correct_when_modify(self):
        """
        Tests does_ref_exists method when a reference is modified
        """
        existing_product = Product(quantity=10, name="Produit1",
                                   price=10.2,
                                   ref="Ref", department=self.department1)
        existing_product.save()
        new_ref = "newRef"
        result = existing_product.is_ref_valid(new_ref)
        self.assertEqual(True, result)


class UnitTestsEmployee(TestCase):
    """
    Unit tests on Employee object
    """

    username1 = "user1"
    password1 = "pass1"

    username2 = "user2"
    password2 = "pass2"

    super_username = "superuser"
    super_password = "superpass"

    employee2 = None
    department3 = None

    @classmethod
    def setUpTestData(cls):
        """
        Generate data for testing
        """
        superuser = User.objects.create_superuser(cls.super_username,
                                                  "fakemail2@mail.com",
                                                  cls.super_password)
        superuser.save()
        store = Store(name="store", user=superuser)
        store.save()
        department1 = Department(name="d1", store=store)
        department1.save()
        department2 = Department(name="d2", store=store)
        department2.save()
        cls.department3 = Department(name="d3", store=store)
        cls.department3.save()
        cls.user1 = User.objects.create_user(cls.username1,
                                             "fakemail@mail.com",
                                             cls.password1)
        cls.user1.save()
        cls.employee1 = Employee(user=cls.user1, phone_number="0505050505",
                                 department=department1)
        cls.employee1.save()
        cls.user2 = User.objects.create_user(cls.username2,
                                             "fakemail@mail.com",
                                             cls.password2)
        cls.user2.save()
        cls.employee2 = Employee(user=cls.user2, phone_number="0505050506",
                                 department=department2)
        cls.employee2.save()

    def test_modify_user_username_ok(self):
        """
        Test modify with correct username
        """
        new_username = "toto"
        self.employee1.user.username = new_username
        self.employee1.modify(user=self.employee1.user)
        self.assertEqual(new_username, self.employee1.user.username)

    def test_modify_user_existing_username_ko(self):
        """
        Test modify with incorrect existing username
        """
        new_username = self.employee2.user.username
        self.employee1.user.username = new_username
        self.assertRaisesMessage(ValueError, "nom d'utilisateur invalide",
                                 self.employee1.modify,
                                 user=self.employee1.user)

    def test_modify_phone_number_ok(self):
        """
        Test modify with correct phone number
        """
        new_phone_number = "0101010101"
        self.employee1.modify(phone_number=new_phone_number)
        self.assertEqual(new_phone_number, self.employee1.phone_number)

    def test_modify_department_ok(self):
        """
        Test modify with correct department
        """
        new_dep = self.department3
        self.employee1.modify(department=new_dep)
        self.assertEqual(new_dep, self.employee1.department)

    def test_does_name_exists_correct(self):
        """
        Tests does_name_exists with valid name
        """
        result = self.employee2.is_name_valid(self.username2)
        self.assertEqual(True, result)

    def test_does_name_exists_incorrect(self):
        """
        Tests does_name_exists with a name that already exists
        """
        result = self.employee2.is_name_valid(self.username1)
        self.assertEqual(False, result)

    def test_does_name_exists_correct_after_modify(self):
        """
        Tests does_name_exists when a name is modified
        """
        self.employee2.user.username = "newName"
        result = self.employee2.is_name_valid(self.employee2.user.username)
        self.assertEqual(True, result)
        self.employee2.user.username = self.username2


class UnitTestsDepartment(TestCase):
    """
    Unit tests on Department object
    """
    dep1_name = "d1"
    department3 = None

    @classmethod
    def setUpTestData(cls):
        """
        Generate data for testing
        """
        superuser = User.objects.create_superuser("super",
                                                  "fakemail2@mail.com",
                                                  "mdp")
        superuser.save()
        store = Store(name="store", user=superuser)
        store.save()
        cls.department1 = Department(name=cls.dep1_name, store=store)
        cls.department1.save()
        department2 = Department(name="d2", store=store)
        department2.save()
        cls.department3 = Department(name="d3", store=store)
        cls.department3.save()

    def test_modify_name_ok(self):
        """
        Test modify with correct name
        """
        new_name = "departement3"
        self.department3.modify(name=new_name)
        self.assertEqual(new_name, self.department3.name)

    def test_modify_name_empty_ko(self):
        """
        Test modify with incorrect empty name
        """
        new_name = ""
        self.assertRaisesMessage(ValueError, "nom vide",
                                 self.department3.modify,
                                 name=new_name)

    def test_modify_name_exiting_ko(self):
        """
        Test modify with incorrect existing name
        """
        new_name = self.department1.name
        self.assertRaisesMessage(ValueError, "nom déjà utilisé",
                                 self.department3.modify,
                                 name=new_name)

    def test_does_name_exists_correct(self):
        """
        Tests does_name_exists method with a correct name
        """
        result = self.department3.is_name_valid(self.department3.name)
        self.assertEqual(True, result)

    def test_does_name_exists_incorrect(self):
        """
        Tests does_name_exists method with a name that already exists
        """
        result = self.department3.is_name_valid(self.dep1_name)
        self.assertEqual(False, result)

    def test_does_name_exists_correct_after_modify(self):
        """
        Tests does_name_exists method with a modified name
        """
        self.department3.name = "new_name"
        result = self.department3.is_name_valid(self.department3.name)
        self.assertEqual(True, result)
