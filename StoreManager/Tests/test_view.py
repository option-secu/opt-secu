"""
This file contains some views tests implemented in
Django
"""

from django.test import TestCase
from django.contrib.auth.models import User
from django.urls import reverse

from StoreManager.models import Employee, Department, Store, Product


class ProductViewTest(TestCase):
    """
    This class contains some tests of the produit view
    """

    username1 = "user1"
    password1 = "pass1"

    super_username = "superuser"
    super_password = "superpass"

    @classmethod
    def setUpTestData(cls):
        """
        Generate data for testing
        """
        superuser = User.objects.create_superuser(cls.super_username,
                                                  "fakemail2@mail.com",
                                                  cls.super_password)
        superuser.save()
        store = Store(name="store", user=superuser)
        store.save()
        department1 = Department(name="d1", store=store)
        department1.save()

        user1 = User.objects.create_user(cls.username1, "fakemail@mail.com",
                                         cls.password1)
        user1.save()
        employee1 = Employee(user=user1, phone_number="0505050505",
                             department=department1)
        employee1.save()

    def test_access_product_view_not_connected(self):
        """
        Tests when an user tries to connect to the produit view without being
        logged in
        """
        response = self.client.get(reverse('produit'))
        self.assertEqual(response.status_code,
                         302)  # la requête s'est bien déroulée
        self.assertRedirects(response,
                             '/StoreManager/connexion?next=/StoreManager/produit')

    def test_access_product_view_connected(self):
        """
        Tests when an user try to connect to the produit view when logged in
        """
        self.client.login(username=self.username1, password=self.password1)
        response = self.client.get(reverse('produit'))
        self.assertEqual(response.status_code,
                         200)  # la requête s'est bien déroulée
        self.assertContains(response, "Bienvenue, " + self.username1 + " !")

    def test_add_product(self):
        """
        Tests when an user add a product
        """
        self.client.login(username=self.username1, password=self.password1)
        num_product = len(Product.objects.all())
        response = self.client.post(reverse('produit'),
                                    {'add': ['Ajouter une ligne']})
        self.assertEqual(response.status_code,
                         200)  # la requête s'est bien déroulée
        self.assertEqual(num_product + 1, len(Product.objects.all()))


class ConnexionViewTests(TestCase):
    """
    This class contains some tests of the connexion view
    """
    username = "fakeusername"
    password = "fakepassword"

    @classmethod
    def setUpTestData(cls):
        """
        Generate data for testing
        """
        superuser = User.objects.create_superuser("boss",
                                                  "fakemail2@mail.com",
                                                  "bossmdp")
        superuser.save()
        store = Store(name="store", user=superuser)
        store.save()
        department = Department(name="d1", store=store)
        department.save()
        user = User.objects.create_user(cls.username, "fakemail@mail.com",
                                        cls.password)
        user.save()
        employee = Employee(user=user, phone_number="0505050505",
                            department=department)
        employee.save()

    def test_connexion_fail(self):
        """ Tests bad login and password"""
        data = {
            'username': 'baduser',
            'password': 'Badpass',
        }
        response = self.client.post(reverse('connexion'), data)

        self.assertEqual(response.status_code,
                         200)  # la requête s'est bien déroulée
        self.assertContains(response,
                            "Utilisateur inconnu ou mauvais mot de passe.")

    def test_connexion_mauvais_mot_de_passe(self):
        """ Tests bad password"""
        data = {
            'username': self.username,
            'password': "badpassword",
        }
        response = self.client.post(reverse('connexion'), data)
        self.assertEqual(response.status_code,
                         200)  # la requête s'est bien déroulée
        self.assertContains(response,
                            "Utilisateur inconnu ou mauvais mot de passe.")

    def test_connexion_mauvais_identifiant(self):
        """ Tests bad login"""
        data = {
            'username': "badId",
            'password': self.password,
        }
        response = self.client.post(reverse('connexion'), data)
        self.assertEqual(response.status_code,
                         200)  # la requête s'est bien déroulée
        self.assertContains(response,
                            "Utilisateur inconnu ou mauvais mot de passe.")

    def test_connexion_reussie(self):
        """ Tests good login and password"""
        data = {
            'username': self.username,
            'password': self.password,
        }
        response = self.client.post(reverse('connexion'), data)

        self.assertEqual(response.status_code,
                         200)  # la requête s'est bien déroulée
        self.assertNotContains(response,
                               "Utilisateur inconnu ou mauvais mot de passe.")
        self.assertContains(response, "Vous êtes connecté,")

    def test_deconnexion(self):
        """ Tests deconnexion"""
        response = self.client.get(reverse('deconnexion'))

        self.assertEqual(response.status_code,
                         302)  # la requête s'est bien déroulée
        self.assertRedirects(response, reverse('connexion'))
