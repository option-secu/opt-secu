"""
This module contains the methods that defines the views
"""
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.conf import settings

# Create your views here.
from django.urls import reverse
from django.contrib.auth.decorators import login_required

import stripe

from StoreManager.models import Product, Department, User, CartElement
from StoreManager.models import Employee
from . import forms

stripe.api_key = settings.STRIPE_SECRET_KEY


def index(request):
    """
    Index view
    """
    return HttpResponse("Coucou Djangooooooooooooooooo !")


@login_required
def produit(request):
    """
    Product view, the user can add, modify and delete products
    """

    error = False
    error_message = ""
    cart_message = None
    superuser = request.user.is_superuser
    if request.method == 'POST' and request.POST.get(
            'add'):  # check if post request comes from correct button
        if not Product.objects.filter(name="").exists():
            if superuser:
                try:
                    default_dep = Department.objects.get(name="")
                except ObjectDoesNotExist:
                    default_dep = Department(name="", store_id="1")
                    default_dep.save()
                new_product = Product(name="", quantity=0, ref="", price=0.0,
                                      department_id=default_dep.id)
            else:
                department = request.user.employee.department
                new_product = Product(name="", quantity=0, ref="", price=0.0,
                                      department=department)
            if not error:
                new_product.save()
        else:
            error = True
            error_message = "un produit par défaut existe déjà"

    elif request.method == 'POST' and request.POST.get(
            'modify'):  # check if post request comes from correct button

        selected_products_id = request.POST.getlist('action_product')

        id_products = list(Product.objects.values_list('id', flat=True))
        id_products.sort()

        name_products = request.POST.getlist('name_product')
        price_products = request.POST.getlist('price_product')
        quantity_products = request.POST.getlist('quantity_product')
        ref_products = request.POST.getlist('ref_product')

        if superuser:
            departments = request.POST.getlist('department')

        for product_id in selected_products_id:
            product = Product.objects.get(pk=product_id)
            if not superuser \
                    and product.department.id \
                    is not request.user.employee.department.id:
                error = True
                error_message = "Vous ne pouvez pas modifier " \
                                "tous les produits séléctionnés" \
                                " (mauvais département)"
            else:
                product_name = name_products[
                    id_products.index(int(product_id))]
                product_price = price_products[
                    id_products.index(int(product_id))]
                product_quantity = quantity_products[
                    id_products.index(int(product_id))]
                product_ref = ref_products[id_products.index(int(product_id))]

                if superuser:
                    department_name = departments[
                        id_products.index(int(product_id))]
                    try:
                        product_department = \
                            Department.objects.get(name=department_name)
                        product.modify(product_quantity, product_name,
                                       product_price, product_ref,
                                       product_department)
                    except ObjectDoesNotExist:
                        error = True
                        error_message = "le département n'existe pas"
                    except ValueError as inst:
                        error = True
                        error_message = inst.args[0]
                else:
                    try:
                        product.modify(product_quantity, product_name,
                                       product_price, product_ref)
                    except ValueError as inst:
                        error = True
                        error_message = inst.args[0]

    elif request.method == 'POST' and request.POST.get(
            'delete'):  # check if post request comes from correct button

        selected_products = request.POST.getlist(
            'action_product')  # get id of selected products
        for product in Product.objects.filter(id__in=selected_products):
            if product.department.id \
                    is not request.user.employee.department.id \
                    and not superuser:
                error = True
                error_message = "Vous ne pouvez pas supprimer " \
                                "tous les produits séléctionnés " \
                                "(mauvais département)"
            else:
                product.delete()

    elif request.method == 'POST' and request.POST.get(
            'cart'):  # check if post request comes from correct button

        selected_products = request.POST.getlist(
            'action_product')  # get id of selected products
        for product in Product.objects.filter(id__in=selected_products):
            if len(CartElement.objects.filter(user=request.user,
                                              product=product)) == 0:
                cart_element = CartElement(product=product,
                                           user=request.user,
                                           quantity=1)
                cart_element.save()
                cart_message = "Les éléments séléctionnés ont été ajoutés au panier !"
            else:
                cart_message = "Certains éléments séléctionnés sont déjà dans le panier"

    header = ['Action', 'Nom', 'Prix', 'Quantité', 'Ref', 'Nom Rayon']
    query_results = Product.objects.all()
    return render(request, 'StoreManager/produit.html',
                  {'username': request.user.username, 'header': header,
                   'data': query_results, 'error': error,
                   'error_message': error_message, 'superuser': superuser,
                   'cart_message': cart_message})


@login_required
def rayon(request):
    """
    Department view, the superuser can add, delete and modify department and
    change the department manager
    """
    error = False
    error_message = ""
    if request.user.is_superuser:
        if request.method == 'POST' and request.POST.get(
                'add'):  # check if post request comes from correct button
            if not Department.objects.filter(name="").exists():
                new_dep = Department(name="", store_id=1)
                new_dep.save()
            else:
                error = True
                error_message = "un rayon par défaut existe déjà"
        elif request.method == 'POST' and request.POST.get(
                'modify'):  # check if post request comes from correct button

            selected_dep_id = request.POST.getlist('action_dep')

            id_deps = list(Department.objects.values_list('id', flat=True))

            dep_names = request.POST.getlist('dep_name')

            usernames = request.POST.getlist('username')

            for dep_id in selected_dep_id:
                dep = Department.objects.get(pk=dep_id)
                dep_name = dep_names[id_deps.index(int(dep_id))]

                try:
                    dep.modify(name=dep_name)
                except ValueError as inst:
                    error = True
                    error_message = inst.args[0]

                if usernames[id_deps.index(int(dep_id))]:
                    try:
                        user = User.objects.get(
                            username=usernames[id_deps.index(int(dep_id))])
                        employee = Employee.objects.get(user=user)
                        employee.department = dep
                        employee.save()
                    except ObjectDoesNotExist:
                        error = True
                        error_message = "l'employé n'existe pas"

        elif request.method == 'POST' and request.POST.get(
                'delete'):  # check if post request comes from correct button

            selected_dep = request.POST.getlist(
                'action_dep')  # get id of selected products
            Department.objects.filter(
                id__in=selected_dep).delete()  # delete selected product

        header = ['Action', 'Rayon', "Nom d'utilisateur"]
        query_employee_results = Employee.objects.all().select_related()
        query_dept = Department.objects.all()

        custom_data = []

        for dept in query_dept:
            for emp in query_employee_results:
                if emp.department is not None and dept.name == emp.department.name:
                    custom_data.append([dept, emp])
            if len(custom_data) == 0 or custom_data[-1][0].name != dept.name:
                custom_data.append([dept, ])
        return render(request, 'StoreManager/rayon.html',
                      {'username': request.user.username, 'header': header,
                       'data': custom_data, 'error': error,
                       'error_message': error_message})
    return render(request, 'StoreManager/forbiddenAccess.html', locals())


@login_required
def employe(request):
    """
    Employee view, the superuser can add, delete and modify other employees
    """
    error = False
    error_message = ""

    if request.user.is_superuser:
        if request.method == 'POST' and request.POST.get(
                'add'):  # check if post request comes from correct button
            if not User.objects.filter(username="default_username").exists():
                new_user = User.objects.create_user(
                    username="default_username", password="password", email="")
                new_user.save()
                try:
                    default_dep = Department.objects.get(name="")
                except ObjectDoesNotExist:
                    default_dep = Department(name="", store_id="1")
                    default_dep.save()
                new_emp = Employee(user=new_user, phonenumber="",
                                   department_id=default_dep.id)
                new_emp.save()
            else:
                error, error_message = True, "un employé par défaut" \
                                             " existe déjà"
        elif request.method == 'POST' and request.POST.get(
                'modify'):  # check if post request comes from correct button

            selected_emp_id = request.POST.getlist('action_user')

            id_employees = list(Employee.objects.values_list('id', flat=True))
            id_employees.sort()

            username = request.POST.getlist('name_user')
            password = request.POST.getlist('password_user')
            phone = request.POST.getlist('phone_user')
            email = request.POST.getlist('email_user')
            dep = request.POST.getlist('dept_user')

            for emp_id in selected_emp_id:
                emp = Employee.objects.get(pk=emp_id)

                emp.user.username = username[id_employees.index(
                    int(emp_id))]

                emp.user.set_password(
                    password[id_employees.index(int(emp_id))])
                emp_phone_number = phone[id_employees.index(int(emp_id))]
                emp.user.email = email[id_employees.index(int(emp_id))]
                emp_department = None
                try:
                    emp.department = Department.objects.get(
                        name=dep[id_employees.index(int(emp_id))])
                    if emp.department.name == "":
                        error, error_message = True, "département invalide"
                        emp_department = None
                except ObjectDoesNotExist:
                    error, error_message = True, "département invalide"
                try:
                    emp.modify(user=emp.user, phone_number=emp_phone_number,
                               department=emp_department)
                except ValueError as inst:
                    error = True
                    error_message = inst.args[0]

        elif request.method == 'POST' and request.POST.get(
                'delete'):  # check if post request comes from correct button

            selected_emp = request.POST.getlist('action_user')
            employees_to_delete = Employee.objects.filter(
                id__in=selected_emp).select_related()

            for employee in employees_to_delete:
                employee.user.delete()
                employee.delete()

        header = ['Action', "Nom d'utilisateur", 'Mot de passe', 'Téléphone',
                  'Email', 'Rayon',
                  'Date de création']
        query_employee_results = Employee.objects.all().select_related()

        return render(request, 'StoreManager/employee.html',
                      {'username': request.user.username, 'header': header,
                       'data': query_employee_results,
                       'error': error,
                       'error_message': error_message})
    return render(request, 'StoreManager/forbiddenAccess.html', locals())


def panier(request):
    """
    Cart view
    Display the cart element of the user
    The user can remove and modify the quantities of the products
    The user can validate the command and proceed to the payment by typing the
    card numbers
    We use the Stripe API to secure and facilitate the payments
    """
    error = False
    validated = False
    price = None
    formatted_price = None
    if request.method == "POST" and request.POST.get(
            'buy'):  # check if post request comes from correct button:
        price = 0
        quantities = request.POST.getlist('quantity')
        cart_element_list = CartElement.objects.filter(user=request.user)
        if len(cart_element_list) > 0:
            for i, cart_element in enumerate(cart_element_list):
                cart_element.quantity = int(quantities[i])
                cart_element.save()
                price += cart_element.product.price * cart_element.quantity

            formatted_price = int(100 * price)
            validated = True
        else:
            validated = False
            error = True
            error_message = "votre panier est vide."

    if request.method == "POST" and request.POST.get(
            'delete'):
        selected_products = request.POST.getlist(
            'action_product')  # get id of selected products
        print(len(selected_products))
        for product in CartElement.objects.filter(id__in=selected_products):
            product.delete()

    if request.method == "POST" and request.POST.get(
            'checkout'):
        print("c'est bien du checkout")
        return HttpResponseRedirect('/')

    key = settings.STRIPE_PUBLISHABLE_KEY
    header = ['Action', 'Nom', 'Prix', 'Quantité']
    data = CartElement.objects.filter(user=request.user)
    return render(request, 'StoreManager/cart.html', locals())


def charge(request, amount):
    """
    Charge view
    Proceed to the payment and remove the bought products from the cart
    """
    if request.method == 'POST':
        stripe.Charge.create(
            amount=int(amount),
            currency='eur',
            description='A Django charge',
            source=request.POST['stripeToken']
        )
        paid_amount = amount / 100
        username = request.user.username
        bought_cart_elements = CartElement.objects.filter(user=request.user)
        for elem in bought_cart_elements:
            elem.delete()

        return render(request, 'StoreManager/charge.html', locals())
    else:
        return None
        # TODO A tester


def connexion(request):
    """Connection view,
    you can enter your login and your password"""
    error = False
    if request.method == "POST":
        form = forms.ConnexionForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username,
                                password=password)  # Nous vérifions si les données sont correctes
            if user:  # Si l'objet renvoyé n'est pas None
                login(request, user)  # nous connectons l'utilisateur
                if 'next' in request.GET:
                    next_ulr = request.GET['next']
                    if next_ulr is not None:
                        return redirect(next_ulr)
            else:  # sinon une erreur sera affichée
                error = True
    else:
        form = forms.ConnexionForm()

    return render(request, 'StoreManager/login.html', locals())


def deconnexion(request):
    """"Disconnection view, when an user access to that view, they will be
    automatically disconnected and redirected to the connection view"""
    logout(request)
    return redirect(reverse(connexion))
