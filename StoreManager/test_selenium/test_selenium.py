import unittest
import time
from selenium import webdriver

'''
    Test case: connexion as admin
'''


class Connexion(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(
            r'chromedriver.exe')

    def test_connexion_admin(self):
        # Connexion as admin user
        driver = self.driver
        driver.get("http://127.0.0.1:8000/StoreManager/connexion")

        element = driver.find_element_by_xpath('//*[@id="id_username"]')
        element.send_keys('admin')

        element = driver.find_element_by_xpath('//*[@id="id_password"]')
        element.send_keys('admin')

        element = driver.find_element_by_xpath(
            '/html/body/form/input[2]').click()
        time.sleep(2)

        assert "Vous êtes connecté, admin !" in driver.page_source

        element = driver.find_element_by_css_selector('body > p:nth-child(3) > a').click()
        time.sleep(2)

        element = driver.find_element_by_xpath(
            '/html/body/header/form/input').click()  # Click deconnexion
        time.sleep(2)

        assert "Nom d'utilisateur:" in driver.page_source

    def test_connexion_employee(self):
        # Connexion as admin user
        driver = self.driver
        driver.get("http://127.0.0.1:8000/StoreManager/connexion")

        element = driver.find_element_by_xpath('//*[@id="id_username"]')
        element.send_keys('Michel')

        element = driver.find_element_by_xpath('//*[@id="id_password"]')
        element.send_keys('password')

        element = driver.find_element_by_xpath(
            '/html/body/form/input[2]').click()
        time.sleep(2)

        assert "Vous êtes connecté, Michel !" in driver.page_source

        element = driver.find_element_by_xpath('/html/body/p[2]/a').click()
        time.sleep(2)

        element = driver.find_element_by_xpath(
            '/html/body/header/form/input').click()  # Click deconnexion
        time.sleep(2)

        assert "Se connecter" in driver.page_source

    def test_connexion_invalid(self):
        # Connexion as admin user
        driver = self.driver
        driver.get("http://127.0.0.1:8000/StoreManager/connexion")

        element = driver.find_element_by_xpath('//*[@id="id_username"]')
        element.send_keys('invalid')

        element = driver.find_element_by_xpath('//*[@id="id_password"]')
        element.send_keys('invalid')

        driver.find_element_by_xpath(
            '/html/body/form/input[2]').click()
        time.sleep(3)

        assert "Utilisateur inconnu ou mauvais mot de passe." in driver.page_source

    def tearDown(self):
        self.driver.close()


'''
    Test case: connexion then, changing department name
'''


class Departement(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(
            r'chromedriver.exe')

    def test_add_remove_rayon(self):
        '''
        Test to modify the first departement name of departement page to 'football de rue'
        '''
        self.connexion()  # connect as admin

        driver = self.driver
        driver.get("http://127.0.0.1:8000/StoreManager/rayon")

        driver.find_element_by_xpath('//*[@id="button"]/input[1]').click()  # Click on add button
        time.sleep(1)

        # New row
        element = driver.find_element_by_css_selector("#content > div > table > tbody > tr:nth-child(5) > "
                                                      "td:nth-child(1) > input[type=checkbox]")

        # Click on checkbox
        element.click()

        driver.find_element_by_xpath('//*[@id="button"]/input[3]').click()
        time.sleep(2)

        try:
            driver.find_element_by_css_selector("#content > div > table > tbody > tr:nth-child(5) > td:nth-child(1) > "
                                                "input[type=checkbox]")
            assert 1 == 2
        except Exception:
            pass

        self.deconnexion()

    def test_modify_rayon_name(self):
        '''
        Test to modify the first departement name of departement page to 'football de rue'
        '''

        self.connexion()  # connect as admin

        driver = self.driver
        driver.get("http://127.0.0.1:8000/StoreManager/rayon")

        element = driver.find_element_by_css_selector(
            '#content > div > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > input[type=text]')  # find input text element

        old_text = element.get_attribute("value")  # get text value

        new_text = "football de rue"
        driver.execute_script(
            "arguments[0].setAttribute('value', '" + new_text + "')",
            element)  # set new value

        driver.find_element_by_xpath(
            '//*[@id="content"]/div/table/tbody/tr[2]/td[1]/input').click()  # click on checkbox

        driver.find_element_by_xpath(
            '//*[@id="button"]/input[2]').click()  # click on modify button
        time.sleep(2)

        element = driver.find_element_by_css_selector(
            '#content > div > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > input[type=text]')

        new_text = element.get_attribute("value")  # get new dep name modified

        self.assertNotEqual(old_text,
                            new_text)  # we check if the departement name is correctly modify

        self.deconnexion()

    def test_modify_user_name_invalid(self):
        '''
        Test to modify the first departement user from 'Michel' to invalid one
        '''

        self.connexion()  # connect as admin

        driver = self.driver
        driver.get("http://127.0.0.1:8000/StoreManager/rayon")

        element = driver.find_element_by_css_selector(
            '#content > div > table > tbody > tr:nth-child(2) > td:nth-child(3) > div > input[type=text]')  # find input text element

        element.get_attribute("value")  # get text value

        new_text = "invalid"
        driver.execute_script(
            "arguments[0].setAttribute('value', '" + new_text + "')",
            element)  # set new value

        driver.find_element_by_xpath(
            '//*[@id="content"]/div/table/tbody/tr[2]/td[1]/input').click()  # click on checkbox

        driver.find_element_by_xpath(
            '//*[@id="button"]/input[2]').click()  # click on modify button
        time.sleep(2)

        assert 'Erreur : l\'employé n\'existe pas' in driver.page_source

        self.deconnexion()

    def connexion(self):
        # Connection as admin user
        driver = self.driver
        driver.get("http://127.0.0.1:8000/StoreManager/rayon")

        element = driver.find_element_by_xpath('//*[@id="id_username"]')
        element.send_keys('admin')

        element = driver.find_element_by_xpath('//*[@id="id_password"]')
        element.send_keys('admin')

        element = driver.find_element_by_xpath(
            '/html/body/form/input[2]').click()
        time.sleep(2)

    def deconnexion(self):
        driver = self.driver

        driver.find_element_by_xpath(
            '/html/body/header/form/input').click()  # Click deconnexion
        time.sleep(2)

        assert "Nom d'utilisateur:" in driver.page_source

    def tearDown(self):
        # Setting back old value
        driver = self.driver

        self.connexion()

        element = driver.find_element_by_css_selector(
            '#content > div > table > tbody > tr:nth-child(2) > td:nth-child(2) > div > input[type=text]')  # find input text element

        new_text = "football"
        driver.execute_script(
            "arguments[0].setAttribute('value', '" + new_text + "')",
            element)  # set new value

        driver.find_element_by_xpath(
            '//*[@id="content"]/div/table/tbody/tr[2]/td[1]/input').click()  # click on checkbox

        driver.find_element_by_xpath(
            '//*[@id="button"]/input[2]').click()  # click on modify button
        time.sleep(2)

        self.deconnexion()

        self.driver.close()


if __name__ == "__main__":
    unittest.main()
