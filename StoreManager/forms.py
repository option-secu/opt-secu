"""
Forms used for post method
"""

from django import forms


class ConnexionForm(forms.Form):
    """
    Connexion form used when a user log in
    """
    username = forms.CharField(label="Nom d'utilisateur", max_length=30)
    password = forms.CharField(label="Mot de passe", widget=forms.PasswordInput)
