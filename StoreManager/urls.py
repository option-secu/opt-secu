from django.urls import path
from . import views

urlpatterns = [
    path('index', views.index),
    path(r'produit', views.produit, name='produit'),
    path(r'rayon', views.rayon, name='rayon'),
    path(r'employe', views.employe, name='employe'),
    path(r'connexion', views.connexion, name='connexion'),
    path(r'deconnexion', views.deconnexion, name='deconnexion'),
    path(r'panier', views.panier, name='panier'),
    path(r'charge/<int:amount>', views.charge, name='charge'),
]
