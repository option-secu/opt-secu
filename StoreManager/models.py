"""
Model
"""

from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Employee(models.Model):
    """
    Employee
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=20)
    department = models.ForeignKey('Department', null=True,
                                   on_delete=models.SET_NULL)

    def modify(self, user=None, phone_number=None, department=None):
        """
        Modify an employee and throw an exception if there is an error
        """
        if user is not None:
            if not self.is_name_valid(user.username):
                raise ValueError("nom d'utilisateur invalide")
            self.user = user

        if phone_number is not None:
            self.phone_number = phone_number
        if department is not None:
            self.department = department

        self.user.save()
        self.save()

    def is_name_valid(self, name):
        """
        Check if the username is already used by another user
        """
        same_name_user = User.objects.filter(username=name)
        if len(same_name_user) != 0:
            if len(same_name_user) != 1:
                return False
            if same_name_user[0].id != self.user.id:
                return False
        return True


class CartElement(models.Model):
    user = models.ForeignKey(User, default=None, on_delete=models.CASCADE)
    product = models.ForeignKey('Product', on_delete=models.CASCADE)
    quantity = models.IntegerField()


class Department(models.Model):
    """
    Department
    """
    name = models.CharField(max_length=150)

    store = models.ForeignKey('Store', on_delete=models.CASCADE)

    def modify(self, name=None):
        """
        Modify a department and throw an exception if there is an error
        """
        if name is not None:
            if name == "":
                raise ValueError("nom vide")
            if not self.is_name_valid(name):
                raise ValueError("nom déjà utilisé")
            self.name = name
        self.save()

    def is_name_valid(self, name):
        """
        Check if the name of the department is already used by another department
        """
        same_name_dep = Department.objects.filter(name=name)
        if len(same_name_dep) != 0:
            if len(same_name_dep) > 1:
                return False
            if same_name_dep[0].id != self.id:
                return False
        return True


class Store(models.Model):
    """
    Store
    """
    name = models.CharField(max_length=150)

    user = models.OneToOneField(User, on_delete=models.CASCADE)


class Product(models.Model):
    """
    Product
    """
    quantity = models.IntegerField()
    name = models.CharField(max_length=150)
    price = models.FloatField()
    ref = models.CharField(max_length=150)

    department = models.ForeignKey('Department', on_delete=models.CASCADE)

    def modify(self, quantity=None, name=None, price=None, ref=None,
               department=None):
        """
        Modify a product, checks if the value are correct and throw an
        exception if it's not the case
        """
        if quantity is not None:
            try:
                new_quantity = int(quantity)
            except ValueError:
                raise ValueError('format de la quantité invalide')

            if new_quantity < 0:
                raise ValueError('la quantité ne peut pas être négative')
            self.quantity = new_quantity

        if price is not None:
            try:
                new_price = float(price)
            except ValueError:
                raise ValueError('format du prix invalide')

            if new_price < 0:
                raise ValueError('le prix ne peut pas être négatif')
            self.price = new_price

        if name is not None:
            if name == "":
                raise ValueError("nom vide")
            self.name = name

        if ref is not None:
            if ref == "":
                raise ValueError("reference vide")
            if not self.is_ref_valid(ref):
                raise ValueError("reference déja existante")
            self.ref = ref

        if department is not None:
            self.department = department

        self.save()
        return True

    def is_ref_valid(self, ref):
        """
        Check if the reference is already used by another reference
        """
        same_ref_products = Product.objects.filter(ref=ref)
        if len(same_ref_products) != 0:
            if len(same_ref_products) > 1:
                return False
            if same_ref_products[0].id != self.id:
                return False
        return True
